#!/bin/bash

set -o errexit -o pipefail -o nounset

if (( $# != 1 )); then
    >&2 echo -n "\
Usage: $0 <gitlab_token>

Uploads the ISO at <repo_root>/out/minikube-amd64.iso.

You can create a GitLab personal access token at:
https://gitlab.com/-/user_settings/personal_access_tokens
"
    exit 2
fi

script_dir="$( realpath -e "$0" | xargs dirname )"
repo_root="$( realpath -e "$script_dir/.." )"

group=subprovisioner
project=minikube
iso_file_base_name=minikube-amd64

token=$1

response=$(
    curl \
        --header "PRIVATE-TOKEN: $token" \
        --upload-file "$repo_root/out/minikube-amd64.iso" \
        "https://gitlab.com/api/v4/projects/$group%2F$project/packages/generic/$iso_file_base_name/1/$iso_file_base_name.iso?select=package_file"
    )

id=$( jq -r .id <<< "$response" )
package_id=$( jq -r .package_id <<< "$response" )

echo
echo "Upload successful, ISO available at:"
echo
echo "    https://gitlab.com/$group/$project/-/package_files/$id/download"
echo
echo "See a list of all uploaded ISOs at:"
echo
echo "    https://gitlab.com/$group/$project/-/packages/$package_id"
