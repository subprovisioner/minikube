################################################################################
#
# sanlock
#
################################################################################

SANLOCK_VERSION = 3.9.1
SANLOCK_SOURCE = sanlock-$(SANLOCK_VERSION).tar.gz
SANLOCK_SITE = https://releases.pagure.org/sanlock
SANLOCK_LICENSE = GPLv2, GPLv2+, LGPLv2+
SANLOCK_LICENSE_FILES = README.license
SANLOCK_INSTALL_STAGING = YES
SANLOCK_DEPENDENCIES = libaio util-linux

define SANLOCK_BUILD_CMDS
	$(TARGET_MAKE_ENV) $(MAKE) CC=$(TARGET_CC) -C $(@D)
endef

define SANLOCK_INSTALL
	# Don't install Python libraries as they may not be needed
	$(TARGET_MAKE_ENV) $(MAKE) -C $(@D)/wdmd DESTDIR=$(1) install
	$(TARGET_MAKE_ENV) $(MAKE) -C $(@D)/src DESTDIR=$(1) install
	$(TARGET_MAKE_ENV) $(MAKE) -C $(@D)/reset DESTDIR=$(1) install
endef

define SANLOCK_INSTALL_STAGING_CMDS
	$(call SANLOCK_INSTALL,$(STAGING_DIR))
endef

define SANLOCK_INSTALL_TARGET_CMDS
	$(call SANLOCK_INSTALL,$(TARGET_DIR))
	$(INSTALL) -D -m 0644 $(@D)/src/logrotate.sanlock $(TARGET_DIR)/etc/logrotate.d/sanlock
	$(INSTALL) -D -m 0644 $(@D)/src/sanlock.conf $(TARGET_DIR)/etc/sanlock/sanlock.conf
	$(INSTALL) -Dd -m 0755 $(TARGET_DIR)/etc/wdmd.d
	$(INSTALL) -Dd -m 0775 $(TARGET_DIR)/var/run/sanlock
	$(INSTALL) -Dd -m 0775 $(TARGET_DIR)/var/run/sanlk-resetd
endef

define SANLOCK_INSTALL_INIT_SYSTEMD
	$(INSTALL) -D -m 0644 $(@D)/init.d/sanlock.service.native $(TARGET_DIR)/usr/lib/systemd/system/sanlock.service
	$(INSTALL) -D -m 0755 $(@D)/init.d/systemd-wdmd $(TARGET_DIR)/usr/lib/systemd/systemd-wdmd
	$(INSTALL) -D -m 0644 $(@D)/init.d/wdmd.service $(TARGET_DIR)/user/lib/systemd/system/wdmd.service
	$(INSTALL) -D -m 0644 $(@D)/init.d/sanlk-resetd.service $(TARGET_DIR)/user/lib/systemd/system/sanlk-resetd.service
endef

$(eval $(generic-package))
