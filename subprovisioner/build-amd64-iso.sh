#!/bin/bash

set -o errexit -o pipefail -o nounset

if (( $# != 0 )); then
    >&2 echo "Usage: $0"
    exit 2
fi

script_dir="$( realpath -e "$0" | xargs dirname )"
repo_root="$( realpath -e "$script_dir/.." )"

make -C "$repo_root" buildroot-image

# --privileged avoids selinux issues with mounting the repo into the container.
# --net=host avoids "connect: cannot assign requested address" errors.
#
# This actually creates the ISO at out/minikube-amd64.iso.
ISO_DOCKER_EXTRA_ARGS='--privileged --net=host' \
    make -C "$repo_root" out/minikube-x86_64.iso

echo
echo "Build successful, ISO available at:"
echo
echo "    $repo_root/out/minikube-amd64.iso"
